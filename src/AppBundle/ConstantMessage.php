<?php

namespace AppBundle;

class ConstantMessage
{
    const HTTP_REQUEST_IS_SUCCESSFUL = 'HTTP request is successful';
    const HTTP_REQUEST_CLIENT_ERROR  = 'HTTP client error';
    const HTTP_REQUEST_SERVER_ERROR  = 'HTTP server error';
    const HTTP_REQUEST_ERROR         = 'HTTP request unknown error';

    const MALFORMED_RESPONSE_ERROR    = 'Malformed JSON error';
    const UNSUCCESSFUL_RESPONSE_ERROR = 'Unsuccessful response error';
}
