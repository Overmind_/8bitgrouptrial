<?php

namespace AppBundle\Client\Exception;

use RuntimeException;

class UnknownErrorException extends RuntimeException
{
}
