<?php

namespace AppBundle\Client\Exception;

use RuntimeException;

class UnsuccessfulResponseException extends RuntimeException
{
}
