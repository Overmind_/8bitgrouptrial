<?php

namespace AppBundle\Client\Exception;

use RuntimeException;

class ClientException extends RuntimeException
{
}
