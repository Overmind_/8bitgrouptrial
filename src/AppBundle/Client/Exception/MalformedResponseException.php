<?php

namespace AppBundle\Client\Exception;

use RuntimeException;

class MalformedResponseException extends RuntimeException
{
}
