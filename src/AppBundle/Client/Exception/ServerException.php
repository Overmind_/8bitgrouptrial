<?php

namespace AppBundle\Client\Exception;

use RuntimeException;

class ServerException extends RuntimeException
{
}
