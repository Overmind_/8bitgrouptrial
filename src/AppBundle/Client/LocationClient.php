<?php
namespace AppBundle\Client;

use AppBundle\Client\Dto\Response;
use AppBundle\Client\Exception\ClientException;
use AppBundle\Client\Exception\MalformedResponseException;
use AppBundle\Client\Exception\ServerException;
use AppBundle\Client\Exception\UnknownErrorException;
use AppBundle\Client\Exception\UnsuccessfulResponseException;
use AppBundle\ConstantMessage;
use Exception;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException as GuzzleClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException as GuzzleServerException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\RequestOptions;
use JMS\Serializer\SerializerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class LocationClient
{
    /** @var \GuzzleHttp\ClientInterface */
    protected $client;

    /** @var SerializerInterface */
    protected $serializer;

    /** @var LoggerInterface */
    protected $logger;

    /**
     * @param ClientInterface     $client
     * @param SerializerInterface $serializer
     * @param LoggerInterface     $logger
     */
    public function __construct(ClientInterface $client, SerializerInterface $serializer, LoggerInterface $logger)
    {
        $this->client     = $client;
        $this->serializer = $serializer;
        $this->logger     = $logger;
    }

    /**
     * @param array $query
     * @param array $context
     *
     * @return \AppBundle\Client\Dto\Location[]
     */
    public function getLocations(array $query = [], array $context = [])
    {
        try {
            $response = $this->client->send($this->createRequest(), $this->getOptions($query));
            $body     = $response->getBody();

            $this->logger->info(
                ConstantMessage::HTTP_REQUEST_IS_SUCCESSFUL,
                array_merge(['response' => $body], $context)
            );

            $response = $this->parseResponse($body, $context);
        } catch (GuzzleClientException $e) {
            $this->logError($e, ConstantMessage::HTTP_REQUEST_CLIENT_ERROR, $context);
            
            throw new ClientException(ConstantMessage::HTTP_REQUEST_CLIENT_ERROR, 0, $e);
        } catch (GuzzleServerException $e) {
            $this->logError($e, ConstantMessage::HTTP_REQUEST_SERVER_ERROR, $context);

            throw new ServerException(ConstantMessage::HTTP_REQUEST_SERVER_ERROR, 0, $e);
        } catch (GuzzleException $e ) {
            $this->logger->error(
                ConstantMessage::HTTP_REQUEST_ERROR,
                array_merge([
                    'message'   => $e->getMessage(),
                    'code'      => $e->getCode(),
                    'exception' => get_class($e),
                ], $context)
            );

            throw new UnknownErrorException(ConstantMessage::HTTP_REQUEST_ERROR, 0, $e);
        }

        return $response->getLocations();
    }

    /**
     * @param BadResponseException $e
     * @param string               $message
     * @param array                $context
     */
    protected function logError(BadResponseException $e, $message, array $context)
    {
        $response         = $e->getResponse();
        $exceptionContext = $this->buildExceptionContext($response, $e);
        $this->logger->error(
            $message,
            array_merge($context, $exceptionContext)
        );
    }

    /**
     * @return Request
     */
    protected function createRequest()
    {
        return new Request(SymfonyRequest::METHOD_GET, '');
    }

    /**
     * @param string $response
     * @param array  $context
     *
     * @return \AppBundle\Client\Dto\ResponseData
     */
    protected function parseResponse($response, array $context)
    {
        try {
            /** @var \AppBundle\Client\Dto\Response $result */
            $result = $this->serializer->deserialize(
                $response,
                Response::class,
                RequestOptions::JSON
            );
        } catch (Exception $e) {
            $this->logger->error(
                ConstantMessage::MALFORMED_RESPONSE_ERROR,
                array_merge($context, [
                    'response' => $response,
                    'exceptionMessage' => $e->getMessage(),
                    'exceptionCode' => $e->getCode(),
                ])
            );
            
            throw new MalformedResponseException(ConstantMessage::MALFORMED_RESPONSE_ERROR, 0, $e);
        }

        if (!$result->isSuccess()) {
            $data    = $result->getData();
            $message = $data->getMessage();
            $code    = $data->getCode();

            $this->logger->error(
                ConstantMessage::MALFORMED_RESPONSE_ERROR,
                array_merge($context, [
                    'message'  => $message,
                    'code'     => $data->getCode(),
                    'response' => $response,
                ])
            );
            
            throw new UnsuccessfulResponseException($message, $code);
        }

        return $result->getData();
    }

    /**
     * @param array $query
     *
     * @return array
     */
    protected function getOptions(array $query)
    {
        return [
            'query' => $query,
        ];
    }

    /**
     * @param ResponseInterface $response
     * @param Exception         $exception
     *
     * @return array
     */
    protected function buildExceptionContext(ResponseInterface $response, Exception $exception)
    {
        $exceptionContext = [
            'code'    => $response->getStatusCode(),
            'body'    => $response->getBody(),
            'message' => $exception->getMessage(),
        ];

        return $exceptionContext;
    }
}
