<?php

namespace AppBundle\Client\Dto;

use JMS\Serializer\Annotation as JMS;

class Response
{
    const SUCCESS_RESPONSE = 'true';

    /**
     * @var ResponseData
     *
     * @JMS\Type("AppBundle\Client\Dto\ResponseData")
     * @JMS\SerializedName("data")
     */
    protected $data;

    /**
     * @var string
     *
     * @JMS\Type("boolean")
     * @JMS\SerializedName("success")
     */
    protected $success;

    /**
     * @return ResponseData
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param ResponseData $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getSuccess()
    {
        return $this->success;
    }

    /**
     * @param string $success
     */
    public function setSuccess($success)
    {
        $this->success = $success;
    }

    /**
     * @return bool
     */
    public function isSuccess()
    {
        $success = $this->getSuccess();

        return is_bool($success) ? $success : ($success == self::SUCCESS_RESPONSE);
    }
}
