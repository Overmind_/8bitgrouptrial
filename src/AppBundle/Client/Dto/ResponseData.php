<?php

namespace AppBundle\Client\Dto;

use JMS\Serializer\Annotation as JMS;

class ResponseData
{
    /**
     * @var Location[]
     *
     * @JMS\Type("array<AppBundle\Client\Dto\Location>")
     * @JMS\SerializedName("locations")
     */
    protected $locations;

    /**
     * @var string
     *
     * @JMS\Type("string")
     * @JMS\SerializedName("message")
     */
    protected $message;

    /**
     * @var string
     *
     * @JMS\Type("string")
     * @JMS\SerializedName("code")
     */
    protected $code;

    /**
     * @return Location[]
     */
    public function getLocations()
    {
        return $this->locations;
    }

    /**
     * @param Location[] $locations
     */
    public function setLocations($locations)
    {
        $this->locations = $locations;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }
}
