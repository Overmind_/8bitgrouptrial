<?php

namespace AppBundle\Client\Dto;

use JMS\Serializer\Annotation as JMS;

class Location
{
    /**
     * @var string
     *
     * @JMS\Type("string")
     * @JMS\SerializedName("name")
     */
    protected $name;

    /**
     * @var Point
     *
     * @JMS\Type("AppBundle\Client\Dto\Point")
     * @JMS\SerializedName("coordinates")
     */
    protected $coordinates;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Point
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }

    /**
     * @param Point $coordinates
     */
    public function setCoordinates($coordinates)
    {
        $this->coordinates = $coordinates;
    }
}
