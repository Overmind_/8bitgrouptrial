<?php

namespace AppBundle\Client\Dto;

use JMS\Serializer\Annotation as JMS;

class Point
{
    /**
     * @var float
     *
     * @JMS\Type("float")
     * @JMS\SerializedName("lat")
     */
    protected $lat;

    /**
     * @var float
     *
     * @JMS\Type("float")
     * @JMS\SerializedName("long")
     */
    protected $long;

    /**
     * @return float
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @param float $lat
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
    }

    /**
     * @return float
     */
    public function getLong()
    {
        return $this->long;
    }

    /**
     * @param float $long
     */
    public function setLong($long)
    {
        $this->long = $long;
    }
}
