<?php

namespace AppBundle\Client;

use AppBundle\Client\Dto\Location;
use AppBundle\Client\Dto\Point;
use AppBundle\Client\Exception\ClientException;
use AppBundle\Client\Exception\MalformedResponseException;
use AppBundle\Client\Exception\UnsuccessfulResponseException;
use AppBundle\ConstantMessage;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class LocationClientTest extends KernelTestCase
{
    /** @var \Symfony\Component\DependencyInjection\ContainerInterface */
    private $container;

    public function setUp()
    {
        self::bootKernel();

        $this->container = self::$kernel->getContainer();
    }

    /**
     * @param array $resultExpected
     *
     * @dataProvider dataGetLocations
     *
     * @group integration
     * @group happy_path
     */
    public function testGetLocations(array $resultExpected)
    {
        $this->assertEquals($resultExpected, $this->getClient()->getLocations());
    }

    public function dataGetLocations()
    {
        return [[
            [
                $this->createLocation('Eiffel Tower', 21.12, 19.56),
                $this->createLocation('Kremlin', 76.54, 49.11),
            ]
        ]];
    }

    /**
     * @param string $message
     * @param string $code
     *
     * @dataProvider dataGetLocationsUnsuccessfulResponse
     *
     * @group integration
     * @group unsuccessful_response
     */
    public function testGetLocationsUnsuccessfulResponse($message, $code)
    {
        $this->expectException(UnsuccessfulResponseException::class);
        $this->expectExceptionMessage($message);
        $this->expectExceptionCode($code);

        $this->getClient()->getLocations();
    }

    public function dataGetLocationsUnsuccessfulResponse()
    {
        return [[
            'Invalid data found',
            '400',
        ]];
    }

    /**
     * @param string $message
     *
     * @dataProvider dataGetLocationsMalformed
     *
     * @group integration
     * @group malformed
     */
    public function testGetLocationsMalformed($message)
    {
        $this->expectException(MalformedResponseException::class);
        $this->expectExceptionMessage($message);

        $this->getClient()->getLocations();
    }

    public function dataGetLocationsMalformed()
    {
        return [[
            ConstantMessage::MALFORMED_RESPONSE_ERROR,
        ]];
    }

    /**
     * @param string $message
     *
     * @dataProvider dataGetLocationsClientError
     *
     * @group integration
     * @group client_error
     */
    public function testGetLocationsClientError($message)
    {
        $this->expectException(ClientException::class);
        $this->expectExceptionMessage($message);

        $this->getClient()->getLocations();
    }

    public function dataGetLocationsClientError()
    {
        return [[
            ConstantMessage::HTTP_REQUEST_CLIENT_ERROR,
        ]];
    }

    /**
     * @param string $name
     * @param float  $latitude
     * @param float  $longitude
     *
     * @return Location
     */
    protected function createLocation($name, $latitude, $longitude)
    {
        $point = new Point();
        $point->setLat($latitude);
        $point->setLong($longitude);

        $location = new Location();
        $location->setName($name);
        $location->setCoordinates($point);

        return $location;
    }

    /**
     * @return \AppBundle\Client\LocationClient
     */
    protected function getClient()
    {
        return $this->container->get('app.client.location');
    }
}
