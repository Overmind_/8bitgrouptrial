Trial task for 8bit Group
=========================

I have prepared some integration tests to check the code. They can be executed as:

``` vendor/bin/phpunit  --group happy_path ```

There are four possible groups in tests: *happy_path, unsuccessful_response, malformed, client_error*
For each group you need to set proper value for parameter ``` location_uri ``` in ``` parameters.yml ```
before running it.


- http://188.226.157.175/happy_path.json
- http://188.226.157.175/unsuccessful_response.json
- http://188.226.157.175/malformed.json
- http://188.226.157.175/not_found.json

